<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group([
    'middleware' => ['auth']
], function () {
    Route::get('home', 'HomeController@index')->name('home');

    //lấy tất cả các messages, và sẽ có form để chat
    Route::get('chat', 'MessageController@index');

    Route::get('messages', 'MessageController@messages');

    //insert chat content vào trong database
    Route::post('messages', 'MessageController@store');

    //lấy ra user hiện tại
    Route::get('current-user', 'UserController@currentUser');
});
