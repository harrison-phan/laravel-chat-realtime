<?php

namespace App\Http\Controllers;

use App\Events\MessagePosted;
use App\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class MessageController extends Controller
{
    public function index()
    {
        return view('chat');
    }

    public function messages()
    {
//        if ($messages = Redis::get('messages.all')) {
//            return json_decode($messages);
//        }
//
//        $messages = Message::with('user')->get();
//        Redis::set('messages.all', $messages);

        return Message::with('user')->get();
    }

    public function store()
    {
        $user = Auth::user();
        $message = Message::create(['message'=> request()->get('message'), 'user_id' => $user->id]);
        broadcast(new MessagePosted($message, $user))->toOthers();

        return $message;
    }
}
