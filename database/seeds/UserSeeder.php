<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->users()->each(function ($user) {
            User::create($user);
        });
    }

    /**
     * Collection of users to create.
     *
     * @return Collection
     */
    private function users()
    {
        $password = Hash::make('secret');

        return collect([
            [
                'name' => 'Test',
                'email' => 'test@chatter.com',
                'password' => $password
            ],
            [
               'name' => 'Admin',
               'email' => 'admin@chatter.com',
               'password' => $password
            ],
        ]);
    }
}
